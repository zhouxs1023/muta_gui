#ifndef MUTA_GUI_H
#define MUTA_GUI_H

#include <stdint.h>

/* ============================================================================
 * Special input characters
 * Place these values in the input text buffer to indicate keypresses.
 * ===========================================================================*/
#define GUI_CHAR_BACKSPACE      (-1)
#define GUI_CHAR_DELETE         (-2)
#define GUI_CHAR_LEFT           (-3)
#define GUI_CHAR_RIGHT          (-4)
#define GUI_CHAR_LONG_LEFT      (-5)
#define GUI_CHAR_LONG_RIGHT     (-6)
#define GUI_CHAR_LONG_BACKSPACE (-7)
#define GUI_CHAR_LONG_DELETE    (-8)
#define GUI_CHAR_LINE_BEGIN     (-9)
#define GUI_CHAR_LINE_END       (-10)

/* ============================================================================
 * Vertex format
 * Each vertex in a draw buffer consists of 20 bytes of data, where:
 * [0] = x;
 * [1] = y;
 * [2] = u;
 * [3] = v;
 * [4] = color (4 * uint8).
 * ===========================================================================*/
#define GUI_NUM_FLOATS_PER_VERT 5
#define GUI_NUM_FLOATS_PER_QUAD (4 * GUI_NUM_FLOATS_PER_VERT)
#define GUI_VERT_SIZE           (GUI_NUM_FLOATS_PER_QUAD * sizeof(float))

#define GUI_MAX_MOUSE_BUTTONS 31

typedef int32_t                             gui_bool_t;
typedef struct gui_init_config_t            gui_init_config_t;
typedef struct gui_input_state_t            gui_input_state_t;
typedef struct gui_draw_cmd_t               gui_draw_cmd_t;
typedef struct gui_draw_list_t              gui_draw_list_t;
typedef struct gui_glyph_t                  gui_glyph_t;
typedef struct gui_font_t                   gui_font_t;
typedef struct gui_border_style_t           gui_border_style_t;
typedef struct gui_win_state_style_t        gui_win_state_style_t;
typedef struct gui_win_style_t              gui_win_style_t;
typedef struct gui_button_state_style_t     gui_button_state_style_t;
typedef struct gui_button_style_t           gui_button_style_t;
typedef struct gui_text_input_state_style_t gui_text_input_state_style_t;
typedef struct gui_text_input_style_t       gui_text_input_style_t;

enum gui_mouse_button
{
    GUI_MOUSE_BUTTON_LEFT     = (1 << 0),
    GUI_MOUSE_BUTTON_RIGHT    = (1 << 1),
    GUI_MOUSE_BUTTON_MIDDLE   = (1 << 2),
    GUI_MOUSE_BUTTON_NONE     = (1 << 31)
};

enum gui_win_flag
{
    GUI_WIN_CLICKTHROUGH = (1 << 0)
};

enum gui_origin
{
    GUI_TOP_LEFT = 0,
    GUI_TOP_RIGHT,
    GUI_TOP_CENTER,
    GUI_BOTTOM_LEFT,
    GUI_BOTTOM_RIGHT,
    GUI_BOTTOM_CENTER,
    GUI_CENTER_LEFT,
    GUI_CENTER_RIGHT,
    GUI_CENTER_CENTER,
    GUI_NUM_ORIGINS
};

enum gui_flip
{
    GUI_FLIP_NONE,
    GUI_FLIP_H,
    GUI_FLIP_V,
    GUI_FLIP_BOTH
};

enum gui_edge
{
    GUI_EDGE_TOP    = 0,
    GUI_EDGE_LEFT   = 1,
    GUI_EDGE_RIGHT  = 2,
    GUI_EDGE_BOTTOM = 3
};

/* ============================================================================
 * enum gui_border
 * Used for styling borders with textures. Borders are styled like so:
 *
 *    top left   top    top right
 *            #--------#
 *            |        |
 *       left |        | right
 *            |        |
 *            #--------#
 * bottom left  bottom  bottom right
 *
 * ===========================================================================*/
enum gui_border
{
    GUI_BORDER_TOP_LEFT = 0,
    GUI_BORDER_TOP_RIGHT,
    GUI_BORDER_BOTTOM_LEFT,
    GUI_BORDER_BOTTOM_RIGHT,
    GUI_BORDER_TOP,
    GUI_BORDER_BOTTOM,
    GUI_BORDER_LEFT,
    GUI_BORDER_RIGHT,
    GUI_NUM_BORDERS
};

enum gui_win_state
{
    GUI_WIN_STATE_INACTIVE = 0,
    GUI_WIN_STATE_HOVERED,
    GUI_WIN_STATE_ACTIVE,
    GUI_NUM_WIN_STATES
};

enum gui_button_state
{
    GUI_BUTTON_STATE_NORMAL = 0,
    GUI_BUTTON_STATE_HOVERED,
    GUI_BUTTON_STATE_PRESSED,
    GUI_NUM_BUTTON_STATES
};

enum gui_text_input_state
{
    GUI_TEXT_INPUT_STATE_INACTIVE = 0,
    GUI_TEXT_INPUT_STATE_HOVERED,
    GUI_TEXT_INPUT_STATE_ACTIVE,
    GUI_NUM_TEXT_INPUT_STATES

};

enum gui_text_input_flag
{
    GUI_TEXT_INPUT_PASSWORD = (1 << 0)
};

struct gui_init_config_t
{
    /* =========================================================================
     * get_texture_dimensions_callback()
     * This callback must take in a pointer to the user's texture structure and
     * write it's width and height in ret_w and ret_h.
     * =======================================================================*/
    void (*get_texture_dimensions_callback)(void *tex, int *ret_w, int *ret_y);

    /* =========================================================================
     * panic_callback()
     * Can be null.
     * Called when a fatal error occurs, for example an assertion failure
     * (assertions are only on in debug mode) or when a memory allocation fails.
     * =======================================================================*/
    void (*panic_callback)(const char *error, ...);
};

/* =============================================================================
 * gui_input_state_t
 * A structure fed to the UI by the user each update.
 * ===========================================================================*/
struct gui_input_state_t
{
    int         mouse_position[2];
    uint32_t    mouse_buttons;          /* A bitmask of mouse buttons held down. */
    int         coordinate_space[4];    /* Rendering coordinate space */
    char        text_input[32];         /* Characters put in this frame. */
    float       delta_time;             /* Delta time is given in seconds. */
};

/* =============================================================================
 * gui_draw_cmd_t
 * Draw command. Use to draw num_verts vertices, the offset is determined by the
 * number of verts in previous draw commands.
 * ===========================================================================*/
struct gui_draw_cmd_t
{
    void    *tex;
    int     num_verts;
    int     scissor[4];
};

/* =============================================================================
 * gui_draw_list_t
 * A list of draw commands. Each window has one of these. All lists are handed
 * to the render function provided  by the user.
 * ===========================================================================*/
struct gui_draw_list_t
{
    gui_draw_cmd_t  *cmds;
    int             num_cmds;
    float           *verts;
    int             num_verts;
};

struct gui_glyph_t
{
    float advance;
    float x_offset;
    float y_offset;
    float clip[4];
    float left_side_bearing;
};

/* =============================================================================
 * gui_font_t
 * A font, initialized with one of the gui_font_load_from_* functions.
 * After loading, the "tex" member of the struct must be initialized by the
 * user.
 * The user can create a texture for the font using the members bitmap, bitmap_w
 * and bitmap_h. The pixels are in RGBA format (1 pixel = 4 bytes).
 * ===========================================================================*/
struct gui_font_t
{
    gui_glyph_t *glyphs;
    int         num_glyphs;
    float       vertical_advance;
    int         height;
    void        *tex;       /* Must be set by the user. */
    void        *bitmap;    /* RGBA bitmap for texture creation */
    int         bitmap_w, bitmap_h;
};

/* =============================================================================
 * gui_border_style_t
 * Indices here accessed with enum gui border. See enum gui_border to see
 * how a style is rendered.
 * ===========================================================================*/
struct gui_border_style_t
{
    int     widths[4]; /* Use enum gui_edge to access by index. */
    uint8_t color[4];
    float   clips[8][4];
    void    *tex;
};

struct gui_win_state_style_t
{
    uint8_t             background_color[4]; /* Background */
    gui_border_style_t  border;
    enum gui_origin     title_origin;
    int                 title_offset[2];
    float               title_scale[2];
    uint8_t             title_color[4];
    gui_font_t          *title_font;
    void                *background_tex;
    float               background_clip[4];
    enum gui_flip       background_flip;
};

struct gui_win_style_t
{
    gui_win_state_style_t states[3]; /* Accessed via enum gui_win_state. */
};

struct gui_button_state_style_t
{
    uint8_t             background_color[4]; /* Background */
    gui_border_style_t  border;
    enum gui_origin     title_origin;
    int                 title_offset[2];
    float               title_scale[2];
    gui_font_t          *font;
};

struct gui_button_style_t
{
    gui_button_state_style_t states[3]; /* Accessed via enum gui_button_state */
};

struct gui_text_input_state_style_t
{
    uint8_t             background_color[4];
    gui_border_style_t  border;
    gui_font_t          *input_font;
    uint8_t             input_color[4];
    float               input_scale;
    int                 input_pixel_offset[2];
    gui_font_t          *title_font;
    uint8_t             title_color[4];
    float               title_scale;
    int                 title_pixel_offset[2];
    enum gui_edge       title_edge;
    enum gui_origin     title_origin;
};

struct gui_text_input_style_t
{
    gui_text_input_state_style_t states[3]; /* Use enum gui_text_input_state */
};

/* =============================================================================
 * Font loading
 * Initialize fonts with the functions gui_font_load_from_file() and
 * gui_font_load_from_memory().
 * After initialization, the user must generate the engine-specific texture of
 * the font. See struct gui_font_t.
 * When fonts are used in any calls to the gui, their memory addresses must stay
 * unchanged until gui_end() is called.
 * ===========================================================================*/

int
gui_font_load_from_file(gui_font_t *font, const char *path, int height);

int
gui_font_load_from_memory(gui_font_t *font, uint8_t *memory, int height);

void
gui_font_destroy(gui_font_t *font);

/* =============================================================================
 * GUI state API
 * ===========================================================================*/

int
gui_init(gui_init_config_t *config);

void
gui_destroy();

/* =============================================================================
 * gui_clear()
 * Remove any existing cached UI objects. Handy when changing scenes.
 * ===========================================================================*/
void
gui_clear();

/* =============================================================================
 * gui_get_input_state()
 * Get a pointer to the gui input state. Call this before gui_begin() and fill
 * up with the application's current input state.
 * Cannot be called between calls to gui_begin() and gui_end();
 * ===========================================================================*/
gui_input_state_t *
gui_get_input_state();

/* =============================================================================
 * gui_begin()
 * Called at the beginning of a new frame. Note: resets all styles back to their
 * defaults.
 * ===========================================================================*/
void
gui_begin();

/* =============================================================================
 * gui_end()
 * Called at the end of a frame. After this, the UI is ready to be rendered.
 * ===========================================================================*/
void
gui_end();

/* =============================================================================
 * gui_get_draw_lists()
 * Get the draw lists generated the last time gui_end() was called. Use to
 * render.
 * ===========================================================================*/
gui_draw_list_t *
gui_get_draw_lists(int *ret_num);

/* =============================================================================
 * gui_get_default_font()
 * A default font is used for gui_text* calls if no other font is set. It will
 * also be used as the font of titles and similar elements in styles by default.
 * At the beginning of the program, the user must generate a texture for this
 * font if it is to be used.
 * ===========================================================================*/
gui_font_t *
gui_get_default_font();

gui_win_style_t *
gui_get_default_win_style();

gui_button_style_t *
gui_get_default_button_style();

uint32_t
gui_get_active_win_id();

/* =============================================================================
 * gui_set_active_win()
 * Sets the given window as the active window. The window must have been drawn
 * at least once before for this function to work. If the title is null,
 * the root window will be used.
 * ==========================================================================*/
void
gui_set_active_win(const char *title);

/* =============================================================================
 * gui_is_any_element_pressed()
 * Returns non-zero if any UI element is currently pressed with a mouse button.
 * ===========================================================================*/
gui_bool_t
gui_is_any_element_pressed();

gui_bool_t
gui_is_any_element_hovered();

gui_bool_t
gui_is_any_button_pressed();

int
gui_current_win_w();

int
gui_current_win_h();

/* =============================================================================
 * gui_begin_win()
 * Begin a new window. End it with gui_end_win(). Any elements, including other
 * windows drawn in between these two calls become children of this window.
 * A hash of the the title is used as an identifier, so it must be unique for
 * each window. To make windows with the same names unique, the signs ## can be
 * used to add unrendered text at the end of the title. For example, the title
 * "Window##abcdefg" renders as "Window".
 * The return value is the window's numerical ID (hash), which can be used with
 * certain other functions.
 * ===========================================================================*/
uint32_t
gui_begin_win(const char *title, int x, int y, int w, int h, int flags);

uint32_t
gui_begin_empty_win(const char *title, int x, int y, int w, int h, int flags);

int
gui_end_win();

/* =============================================================================
 * Begin a rectangle to act as a new point of origin for all gui actions.
 * Relational to the currently active window. If the window ends, the guide
 * stack is wound back to zero.
 * Use this instead of gui_begin_empty_win() when you don't need a separate
 * draw list for the contenst of the rectangle.
 * The guide stack will automatically be terminated when the parent window is
 * ended.
 * ===========================================================================*/
int
gui_begin_guide(int x, int y, int w, int h);

void
gui_end_guide();

uint32_t
gui_repeat_button(const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask);

uint32_t
gui_invisible_repeat_button(const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask);

/* =============================================================================
 * gui_button()
 * Returns non-zero if the mouse button was pressed down and later released on
 * top of the button rectangle.
 * The parameter "mbtn_mask" tells the button which mouse buttons it is to react
 * to. Handly flags are GUI_MOUSE_BUTTON_LEFT, GUI_MOUSE_BUTTON_RIGHT and
 * GUI_MOUSE_BUTTON_MIDDLE.  If "mbtn_mask" is 0, a reaction to the left mouse
 * button is assumed. To make the button non-reactionary, the value
 * GUI_MOUSE_BUTTON_NONE can be used.  The return value is mbtn_mask ANDed with
 * the released mouse flags - hence it can be tested with the proper bitflags
 * if, for example, the button was clicked with the left or the right mouse
 * button.
 * ===========================================================================*/
uint32_t
gui_button(const char *title, int x, int y, int w, int h, uint32_t mbtn_mask);

uint32_t
gui_invisible_button(const char *title, int x, int y, int w, int h,
    uint32_t mbtn_mask);

/* =============================================================================
 * gui_text()
 * Render text. The font used will be the font set with gui_font(), and the
 * colour used will be the colour set with gui_color().
 * The parameter wrap is the maximum width of the text, at which point it will
 * wrap to the next row. If wrap is 0, no wrapping will be used.
 * The neatly render multiple text blocks in a row, the functions
 * gui_get_last_text_h(), gui_get_last_texture_y(), etc. can be used for
 * placement computation.
 * ===========================================================================*/
void
gui_text(const char *text, int wrap, int x, int y);

void
gui_text_s(const char *text, int wrap, int x, int y, float s); /* Scaled text */

/* =============================================================================
 * gui_textf()
 * Render printf-style formatted text.
 * ===========================================================================*/
void
gui_textf(const char *fmt, int wrap, int x, int y, ...);

void
gui_textf_s(const char *fmt, int wrap, int x, int y, float scale, ...);

/* =============================================================================
 * gui_text_input()
 * Returns non-zero if this is the currently active text input. For possible
 * flags, see enum gui_text_input_flag.
 * ===========================================================================*/
gui_bool_t
gui_text_input(const char *title, char *buf, uint32_t buf_size, int x, int y,
    int w, int h, int flags);

gui_bool_t
gui_text_input_enter_pressed();

void
gui_set_active_text_input(const char *title); /* Pass in null to set to none */

gui_bool_t
gui_have_active_text_input();

gui_bool_t
gui_is_text_input_active(const char *title);

/* =============================================================================
 * gui_texture()
 * Render a texture with the specified clip. If clip is null, the whole image
 * will be used.
 * For other variations, the postfixes are:
 * s: scale
 * c: color
 * f: flip
 * r: rotate
 * ===========================================================================*/
int
gui_texture(void  *tex, float *clip, int x, int y);

int
gui_texture_s(void  *tex, float *clip, int x, int y, float sx, float sy);

int
gui_texture_c(void  *tex, float *clip, int x, int y, uint8_t col[4]);

int
gui_texture_f(void  *tex, float *clip, int x, int y, enum gui_flip flip);

int
gui_texture_r(void  *tex, float *clip, int x, int y, float rot);

int
gui_texture_sc(void  *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4]);

int
gui_texture_sf(void  *tex, float *clip, int x, int y, float sx, float sy,
    enum gui_flip flip);

int
gui_texture_sr(void  *tex, float *clip, int x, int y,
    float sx, float sy, float rot);

int
gui_texture_cf(void  *tex, float *clip, int x, int y, uint8_t col[4],
    enum gui_flip flip);

int
gui_texture_cr(void  *tex, float *clip, int x, int y, uint8_t col[4],
    float rot);

int
gui_texture_scf(void  *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4], enum gui_flip flip);

int
gui_texture_scfr(void  *tex, float *clip, int x, int y,
    float sx, float sy, uint8_t col[4], enum gui_flip flip, float rot);

/* =============================================================================
 * gui_rectangle()
 * Render a single-colored rectangle. The colour used will be the one set with
 * gui_color().
 * ===========================================================================*/
int
gui_rectangle(int x, int y, int w, int h);

/* =============================================================================
 * gui_origin()
 * Set the current origin. All coordinates are relative the set origin. The
 * origin is also used as objects' anchor. Reset to GUI_TOP_LEFT on gui_begin().
 * ===========================================================================*/
void
gui_origin(enum gui_origin origin);

/* =============================================================================
 * gui_font()
 * Set the current font. The font pointer must stay valid until gui_end() is
 * called. Reset to the default font on a call to gui_begin().
 * ===========================================================================*/
void
gui_font(gui_font_t *font);

/* =============================================================================
 * gui_color()
 * Set the current draw color. Used for text and shape rendering. Reset to pure
 * white on gui_begin().
 * ===========================================================================*/
void
gui_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

/* =============================================================================
 * gui_win_style()
 * Set the current window style. Pointer must stay valid until gui_end() is
 * called. Reset to default window style on gui_begin().
 * ===========================================================================*/
void
gui_win_style(gui_win_style_t *style); /* If NULL, use default style. */

/* =============================================================================
 * gui_button_style()
 * Set the current button style. Pointer must stay valid until gui_end() is
 * called. Reset to default button style on gui_begin().
 * ===========================================================================*/
void
gui_button_style(gui_button_style_t *style); /* If NULL, use default style. */

/* =============================================================================
 * gui_text_input_style()
 * Set the current text input style. Pointer must stay valid until gui_end() is
 * called. Reset to default text input style on gui_begin().
 * ===========================================================================*/
void
gui_text_input_style(gui_text_input_style_t *style);

/* =============================================================================
 * gui_is_window_active()
 * May be called between gui_begin_win() and gui_end_win(). Returns non-zero if
 * the current window is the active window.
 * ===========================================================================*/
gui_bool_t
gui_is_window_active();

/* =============================================================================
 * gui_is_window_hovered()
 * While inside gui_begin_win() and gui_end_win() calls, call this to check if
 * the window is hovered.
 * ===========================================================================*/
gui_bool_t
gui_is_window_hovered();

/* ============================================================================
 * Getting last element dimensions (gui_get_last_*...)
 * All of the x and y coordinates returned are relative to the origin used at
 * the time of rendering the element. In the functions that return rect as
 * arrays, elements 2 and 3 are width and height.
 * ===========================================================================*/

int
gui_get_last_text_origin();

void
gui_get_last_text_dimensions(int ret_rect[4]);

int
gui_get_last_text_x();

int
gui_get_last_text_y();

int
gui_get_last_text_w();

int
gui_get_last_text_h();

int
gui_get_last_button_origin();

void
gui_get_last_button_dimensions(int ret_rect[4]);

int
gui_get_last_button_x();

int
gui_get_last_button_y();

int
gui_get_last_button_w();

int
gui_get_last_button_h();

int
gui_get_last_texture_origin();

void
gui_get_last_texture_dimensions(int ret_rect[4]);

int
gui_get_last_texture_x();

int
gui_get_last_texture_y();

int
gui_get_last_texture_w();

int
gui_get_last_texture_h();

int
gui_get_num_open_wins();

gui_win_style_t
gui_create_win_style();

gui_button_style_t
gui_create_button_style();

gui_text_input_style_t
gui_create_text_input_style();

/* =============================================================================
 * gui_compute_text_dimensions()
 * Returns the expected width and height of a block of text. Height or width are
 * not clamped to the parent window's width and height.
 * ===========================================================================*/
void
gui_compute_text_dimensions(int *w, int *h, const char *text, gui_font_t *font,
    float scale, int wrap, int window_w, int window_h);

static inline void
gui_set_color_array(uint8_t color[4], uint8_t r, uint8_t g, uint8_t b,
    uint8_t a);

static inline void
gui_set_color_array(uint8_t color[4], uint8_t r, uint8_t g, uint8_t b,
    uint8_t a)
{
    color[0] = r;
    color[1] = g;
    color[2] = b;
    color[3] = a;
}

#endif /* MUTA_GUI_H */
